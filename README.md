# Atlassian Server DevTools

A Browser DevTools plug-in that helps you discover the usage of deprecated HTML and CSS patterns from the [AUI Library](https://docs.atlassian.com/aui).

![Demo](../../raw/master/docs/asd-demo.gif)

## Installation

You can download and install the extensions for one of your browsers:

 - [Chrome extension](https://chrome.google.com/webstore/detail/atlassian-server-devtools/ifdlelpkchpppgmhmidenfmlhdafbnke)
 - [Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/atlassian-server-devtools/)
 - Opera (coming soon)

## How to use it?

 1. Install the extension for your Browser using one of the provided links.
 2. Go to any website that is using AUI library.
 3. Open the DevTools by using the action from Browser menu or by pressing context menu and selecting **Inspect** option.
 4. From the DevTools panel select the **ASD** tab.
 5. If the website is using **AUI** you should be able to see the information about the version on top of the panel:

    ![AUI Version](../../raw/master/docs/aui-version.png)

### Deprecation scanner
After opening **ASD** tab you might see the result of **Deprecation Scanner**. This will give you the verbose output whenever the page is using some of the deprecated HTML or CSS patterns. You can also filter and persist the deprecation results by using built-in actions. 

The depreciation item contains 4 information:

 - Deprecation message with some hints.
 - Deprecation level (background): **Error** (red) or **Warning** (yellow).
 - In what version of AUI a pattern was deprecated. It's also a link to the AUI documentation.
 - HTML node that you can hover the mouse on to see the deprecation highlighter on the page. You can also click on it to scroll website to the place where an HTML node is visible.

### Deprecation levels
There are two deprecation levels: 

 - **Error** (red)
   
   ![Error](../../raw/master/docs/error.png)
   
 - **Warning** (yellow)

   ![Warning](../../raw/master/docs/warning.png)

If the results contain the **Error** deprecation, it means that the HTML or CSS pattern was deprecated in current version of AUI that you are using on the website.

The **Warning** level tells you that this pattern will be deprecated in the newer version of AUI (after upgrading AUI). It helps you to take care of the depreciation even before upgrading the AUI library to the newest version.

const { HotModuleReplacementPlugin } = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const webpackConfig = require('./webpack.config');
const PATHS = require('./config/paths');
const { htmlWebpackPluginConfig } = require('./webpack.config.parts');

const webpackConfigDev = merge.strategy({
  entry: 'replace'
})(webpackConfig, {
  devtool: 'cheap-module-source-maps',

  entry: {
    asd_panel_dev: [
      'react-hot-loader/patch',
      require.resolve('react-dev-utils/webpackHotDevClient'),
      `${PATHS.SRC}/Panel/Panel.dev.jsx`
    ]
  },

  plugins: [
    new HotModuleReplacementPlugin(),

    new HtmlWebpackPlugin(htmlWebpackPluginConfig({
      title: 'ASD DevTools Panel',
      filename: 'asd_panel_dev.html',
      chunks: ['asd_panel_dev']
    }))
  ],

  devServer: {
    contentBase: PATHS.DIST,
    index: 'asd_panel_dev.html',
    compress: true,
    host: '0.0.0.0',
    port: 3000,
    historyApiFallback: true,
    inline: true,
    hot: true,
    overlay: false
  }
});

module.exports = webpackConfigDev;

const path = require('path');

const PATHS = {
  SRC: path.resolve(__dirname, '../src'),
  DIST: path.resolve(__dirname, '../dist')
};

module.exports = PATHS;

const connections = {};

const injectContentScript = (tabId) => {
  console.debug(`B: Injecting Content Script on the page ${tabId}`);

  // TODO: Extract Chrome API for mocking and testing purpose
  chrome.tabs.executeScript(tabId, { file: 'asd_content_script.js' });
};

const findConnectionTabIdByPort = (port) => {
  let foundTabId = null;

  Object.keys(connections).some((tabId) => {
    const foundConnection = Object
      .keys(connections[tabId])
      .some(portKey => connections[tabId][portKey] === port);

    if (foundConnection) {
      foundTabId = tabId;
    }

    return foundTabId;
  });

  return foundTabId;
};

// eslint-disable-next-line
const setIconAndPopup = (isEnabled, tabId) => {
  const popup = `popups/${isEnabled ? 'enabled' : 'disabled'}.html`;

  chrome.browserAction.setPopup({ tabId, popup });
};


const PANEL_TO_CONTENT_SCRIPT_TOPICS = [
  'HIGHLIGHT_NODE',
  'UNHIGHLIGHT_NODE',
  'SCROLL_TO_NODE',
  'TOGGLE_HIGHLIGHTING_NODES',
  'REMOVE_HIGHLIGHTING_ON_ALL_NODES'
];

// TODO: Extract Chrome API for mocking and testing purpose
chrome.runtime.onConnect.addListener((port) => {
  console.debug(`B: Connected to port ${port.name}`);

  const messageListener = ({ topic, ...message }, connectionPort) => {
    console.debug(`B: Received a message ${topic} from "${port.name}" port`, message);

    if (topic === 'INIT_DEVTOOLS') {
      const { tabId } = message;

      connections[tabId] = connections[tabId] || {};
      connections[tabId].devtools = connectionPort;
    } else if (topic === 'INIT_PANEL') {
      const { tabId } = message;

      connections[tabId] = connections[tabId] || {};
      connections[tabId].panel = connectionPort;

      injectContentScript(tabId);
    } else if (topic === 'DEPRECATION') {
      const tabId = connectionPort.sender.tab.id;

      const panelPort = connections[tabId].panel;

      panelPort.postMessage({ topic, ...message });
    } else if (topic === 'INIT_CONTENT_SCRIPT') {
      const tabId = connectionPort.sender.tab.id;

      connections[tabId] = connections[tabId] || {};
      connections[tabId].contentScript = connectionPort;

      const panelPort = connections[tabId].panel;

      panelPort.postMessage({ topic: 'CONTENT_SCRIPT_INJECTED' });
    } else if (PANEL_TO_CONTENT_SCRIPT_TOPICS.includes(topic)) {
      const { tabId } = message;
      const contentScriptPort = connections[tabId].contentScript;

      contentScriptPort.postMessage({ topic, ...message });
    } /* else if (topic === 'AUI_VERSION') {
      const { auiVersion, tabId } = message;
      const hasAui = Boolean(auiVersion);

      setIconAndPopup(hasAui, tabId);
    }
    */
  };

  console.debug(`B: Establishing listener for ${port.name}`);

  port.onMessage.addListener(messageListener);

  port.onDisconnect.addListener((disconnectedPort) => {
    const { name: portName } = disconnectedPort;

    if (portName === 'asd_panel' || portName === 'asd_devtools') {
      console.debug(`B: Disconnecting port "${portName}`, disconnectedPort);

      port.onMessage.removeListener(messageListener);

      const tabId = findConnectionTabIdByPort(port);
      const contentScriptPort = connections[tabId].contentScript;

      contentScriptPort.postMessage({ topic: 'DEVTOOLS_CLOSED' });

      delete connections[tabId];
    }
  });
});

// TODO: Extract Chrome API for mocking and testing purpose
chrome.tabs.onUpdated.addListener((tabId, changedInfo /* , tab */) => {
  console.debug(`B: Tab ${tabId} was reloaded`);

  const connection = connections[tabId];

  if (!connection) {
    console.warn(`B: No connections for tab ${tabId}`);

    return;
  }

  const panelPort = connection.panel;

  if (!panelPort) {
    console.warn(`B: No connections with Panel for tab ${tabId}`);

    return;
  }

  console.debug(`B: Sending reload message from tab ${tabId}`, changedInfo);

  if (changedInfo.status === 'loading') {
    panelPort.postMessage({
      topic: 'LOADING',
      changedInfo
    });
  } else if (changedInfo.status === 'complete') {
    panelPort.postMessage({
      topic: 'LOADED',
      changedInfo
    });
    injectContentScript(tabId); // TODO: Inject Content Script only when opening devtools? Or when page is opened?
  }

  /*
  if (tab.active && changedInfo.status === 'loading') {
    setIconAndPopup(false, tabId);
  }
  */
});

import initDeprecationWatcher from './deprecations/deprecations';
import getAuiVersion from './helpers/auiVersionHelper';

import { initConnection } from './portConnection';
import attachListeners from './attachListeners';
import notifier from './notifiers/notifier';

const AUI_VERSION = getAuiVersion();

if (AUI_VERSION) {
  initConnection();
  attachListeners();

  initDeprecationWatcher(AUI_VERSION, notifier);
}

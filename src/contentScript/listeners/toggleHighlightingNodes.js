import { toggleHighlightAllNodes } from '../actions/deprecationHighlighter';
import store from '../store';

export default attachListener => (
  attachListener('TOGGLE_HIGHLIGHTING_NODES', (message) => {
    const { highlight } = message;

    store.highlightNodes = highlight;

    toggleHighlightAllNodes(highlight);
  })
);

import { highlightNode } from '../actions/deprecationHighlighter';

export default attachListener => (
  attachListener('HIGHLIGHT_NODE', (message) => {
    const { nodeId } = message;

    highlightNode(nodeId);
  })
);

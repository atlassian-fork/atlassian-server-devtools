import { removeHighlightingOnAllNodes } from '../actions/deprecationHighlighter';

export default attachListener => (
  attachListener('REMOVE_HIGHLIGHTING_ON_ALL_NODES', () => {
    removeHighlightingOnAllNodes();
  })
);

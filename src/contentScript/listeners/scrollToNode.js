import scrollToNode from '../actions/scrollToNode';

export default attachListener => (
  attachListener('SCROLL_TO_NODE', (message) => {
    const { nodeId } = message;

    scrollToNode(nodeId);
  })
);

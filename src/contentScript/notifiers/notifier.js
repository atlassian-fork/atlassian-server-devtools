import { ADDED_NODE, REMOVED_NODE } from '../deprecations/watcher';

import { toggleHighlightAllNodes, unhighlightNode } from '../actions/deprecationHighlighter';
import { getNodeId } from '../deprecations/registry';

import deprecationNotifier from './deprecationNotifier';

const notifier = (mutationType, deprecationInfo) => {
  if (mutationType === ADDED_NODE) {
    deprecationNotifier(deprecationInfo);
    toggleHighlightAllNodes();
  } else if (mutationType === REMOVED_NODE) {
    const { node } = deprecationInfo;
    const nodeId = getNodeId(node);

    unhighlightNode(nodeId);
  }
};

export default notifier;

import { createReducer } from 'redux-create-reducer';

import { STORE_DEPRECATION, CLEAR_DEPRECATIONS, FILTER_DEPRECATIONS } from '../constants/deprecations';
import { LOADING, TOGGLE_PRESERVE_LOG } from '../constants/panel';

const defaultState = {
  preserveLog: false,
  deprecations: [],
  filterQuery: null
};

const deprecations = createReducer(defaultState, {
  [LOADING](state) {
    const { preserveLog } = state;

    if (!preserveLog) {
      return { ...state, deprecations: [] };
    }

    return state;
  },

  [STORE_DEPRECATION](state, { deprecation }) {
    return {
      ...state,
      deprecations: [
        ...state.deprecations,
        deprecation
      ]
    };
  },

  [CLEAR_DEPRECATIONS](state) {
    return {
      ...state,
      deprecations: []
    };
  },

  [TOGGLE_PRESERVE_LOG](state) {
    const { preserveLog } = state;

    return {
      ...state,
      preserveLog: !preserveLog
    };
  },

  [FILTER_DEPRECATIONS](state, { filterQuery }) {
    return {
      ...state,
      filterQuery
    };
  }
});

export default deprecations;

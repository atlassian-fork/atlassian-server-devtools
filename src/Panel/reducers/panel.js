import { createReducer } from 'redux-create-reducer';

import { LOADING, LOADED, VERSION, HAS_AUI, TOGGLE_SHOW_TIMESTAMPS } from '../constants/panel';

const defaultState = {
  version: null,
  hasAui: false,
  isLoaded: false,
  showTimestamps: false
};

const panel = createReducer(defaultState, {
  [LOADING](state) {
    return {
      ...state,
      isLoaded: false,
      hasAui: false,
      version: null
    };
  },


  [LOADED](state) {
    return {
      ...state,
      isLoaded: true
    };
  },

  [HAS_AUI](state, { hasAui }) {
    return {
      ...state,
      hasAui
    };
  },

  [VERSION](state, { version }) {
    return {
      ...state,
      version
    };
  },

  [TOGGLE_SHOW_TIMESTAMPS](state) {
    const { showTimestamps } = state;

    return {
      ...state,
      showTimestamps: !showTimestamps
    };
  }
});


export default panel;

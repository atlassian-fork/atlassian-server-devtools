import { connect } from 'react-redux';

import { getFilteredDeprecations } from '../../selectors/deprecations';

const mapStateToProps = state => ({
  deprecations: getFilteredDeprecations(state)
});

export default Component => connect(mapStateToProps)(Component);

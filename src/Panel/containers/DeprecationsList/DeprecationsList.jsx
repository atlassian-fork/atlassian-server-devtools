import React from 'react';
import PropTypes from 'prop-types';

import Console from '../../components/Console/Console';
import EmptyDeprecationsList from '../../components/EmpyDeprecationsList/EmptyDeprecationsList';
import DeprecationMessage from '../DeprecationMessage/DeprecationMessage';

import DeprecationPropType from '../../propTypes/DeprecationPropType';

import withDefaultProps from './withDefaultProps';

const DeprecationsList = ({ deprecations }) => {
  const deprecationMessages = deprecations.map((deprecation) => {
    const { nodeId, selector } = deprecation;
    const key = `${nodeId}-${selector}`;

    return (<DeprecationMessage key={key} deprecation={deprecation} />);
  });

  return (
    <Console emptyOutput={<EmptyDeprecationsList />}>
      {deprecationMessages}
    </Console>
  );
};

DeprecationsList.propTypes = {
  deprecations: PropTypes.arrayOf(DeprecationPropType.isRequired).isRequired
};

export default withDefaultProps(DeprecationsList);

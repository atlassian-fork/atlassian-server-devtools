import DeprecationMessage from '../../components/DeprecationMessage/DeprecationMessage';
import withDefaultProps from './withDefaultProps';

export default withDefaultProps(DeprecationMessage);

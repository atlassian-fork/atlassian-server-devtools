import { connect } from 'react-redux';

import { highlightNode, unhighlightNode, scrollToNode } from '../../actions/highlighting';

const mapStateToProps = (state) => {
  const { panel } = state;
  const { version: auiVersion, showTimestamps } = panel;

  return { auiVersion, showTimestamps };
};

const mapDispatchToProps = dispatch => ({
  onHighlight: nodeId => dispatch(highlightNode(nodeId)),

  onUnhighlight: nodeId => dispatch(unhighlightNode(nodeId)),

  onElementClick: nodeId => dispatch(scrollToNode(nodeId))
});

export default Component => connect(mapStateToProps, mapDispatchToProps)(Component);

import { connect } from 'react-redux';

import { togglePreserveLog, toggleShowTimeStamps } from '../../actions/panel';
import { toggleHighlightDeprecatedElements } from '../../actions/highlighting';
import { clearDeprecations, filterDeprecations } from '../../actions/deprecations';

const mapStateToProps = (state) => {
  const { panel, highlighting } = state;
  const { hasAui, version: auiVersion, isLoaded } = panel;
  const { highlightDeprecatedElements } = highlighting;

  return {
    isLoaded,
    auiVersion,
    hasAui,
    highlightDeprecatedElements
  };
};

const mapDispatchToProps = dispatch => ({
  toggleHighlightDeprecatedElements: () => dispatch(toggleHighlightDeprecatedElements()),
  togglePreserveLog: () => dispatch(togglePreserveLog()),
  toggleShowTimeStamps: () => dispatch(toggleShowTimeStamps()),
  clearDeprecations: () => dispatch(clearDeprecations()),
  filterDeprecations: filterQuery => dispatch(filterDeprecations(filterQuery))
});

export default Component => connect(mapStateToProps, mapDispatchToProps)(Component);

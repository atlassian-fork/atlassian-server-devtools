import React from 'react';
import PropTypes from 'prop-types';

import Toolbar from '../../components/Toolbar/Toolbar';
import ToolbarToggleButton from '../../components/ToolbarToggleButton/ToolbarToggleButton';
import ToolbarItem from '../../components/ToolbarItem/ToolbarItem';
import ToolbarDivider from '../../components/ToolbarDivider/ToolbarDivider';
import ToolbarIconButton, { ClearIcon } from '../../components/ToolbarIconButton/ToolbarIconButton';
import ToolbarSpacer from '../../components/ToolbarSpacer/ToolbarSpacer';
import ToolbarField from '../../components/ToolbarField/ToolbarField';

import withDefaultProps from './withDefaultProps';

const PanelToolbar = ({
  highlightDeprecatedElements,
  toggleHighlightDeprecatedElements,
  togglePreserveLog,
  toggleShowTimeStamps,
  clearDeprecations,
  filterDeprecations,
  isLoaded,
  hasAui,
  auiVersion
}) => (
  <Toolbar>
    <ToolbarToggleButton
      selected={highlightDeprecatedElements}
      onToggle={toggleHighlightDeprecatedElements}
    >
      Auto Highlight Deprecations
    </ToolbarToggleButton>

    <ToolbarDivider />

    <ToolbarItem>
      {isLoaded ? ((hasAui && `AUI version: ${auiVersion}`) || 'This page is not using AUI') : 'detecting AUI...'}
    </ToolbarItem>

    <ToolbarDivider />

    <ToolbarField
      placeholder="Filter"
      onChange={value => filterDeprecations(value)}
    />

    <ToolbarSpacer />

    <ToolbarToggleButton
      onToggle={togglePreserveLog}
      title="Do not clear output on page reload / navigation"
    >
      Preserve log
    </ToolbarToggleButton>

    <ToolbarToggleButton onToggle={toggleShowTimeStamps}>
      Show Timestamps
    </ToolbarToggleButton>

    <ToolbarIconButton
      icon={ClearIcon}
      onClick={clearDeprecations}
      title="Clear Deprecation Scanner output"
    />
  </Toolbar>
);

PanelToolbar.propTypes = {
  isLoaded: PropTypes.bool,
  hasAui: PropTypes.bool,
  auiVersion: PropTypes.string,
  highlightDeprecatedElements: PropTypes.bool,
  toggleHighlightDeprecatedElements: PropTypes.func.isRequired,
  togglePreserveLog: PropTypes.func.isRequired,
  toggleShowTimeStamps: PropTypes.func.isRequired,
  clearDeprecations: PropTypes.func.isRequired,
  filterDeprecations: PropTypes.func.isRequired
};

PanelToolbar.defaultProps = {
  isLoaded: true,
  hasAui: false,
  auiVersion: null,
  highlightDeprecatedElements: false
};

export default withDefaultProps(PanelToolbar);

import React from 'react';
import { render } from 'react-dom';

import './portConnection';

import store from './store';
import PanelApp from './containers/PanelApp/PanelApp';

const renderApp = (rootId = 'root') => {
  const targetEl = document.getElementById(rootId);

  render(<PanelApp store={store} />, targetEl);
};

renderApp();

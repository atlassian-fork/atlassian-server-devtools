import { createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';

import reducers from './reducers/reducers';
import attachStoreNotifiers from './topics/attachStoreNotifiers';

const store = createStore(reducers, devToolsEnhancer());

attachStoreNotifiers(store);

export default store;

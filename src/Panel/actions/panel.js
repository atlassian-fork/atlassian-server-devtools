import { VERSION, LOADING, LOADED, HAS_AUI, TOGGLE_SHOW_TIMESTAMPS, TOGGLE_PRESERVE_LOG } from '../constants/panel';

export const hasAui = result => ({
  type: HAS_AUI,
  hasAui: result
});

export const setVersion = version => ({
  type: VERSION,
  version
});

export const isLoading = () => ({
  type: LOADING
});

export const wasLoaded = () => ({
  type: LOADED
});

export const toggleShowTimeStamps = () => ({
  type: TOGGLE_SHOW_TIMESTAMPS
});


export const togglePreserveLog = () => ({
  type: TOGGLE_PRESERVE_LOG
});

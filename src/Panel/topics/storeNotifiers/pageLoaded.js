import { LOADED } from '../contants';
import { hasAui, setVersion, wasLoaded } from '../../actions/panel';
import auiDetector from '../../services/aui/auiDetector';
import getVersion from '../../services/aui/getVersion';

const pageLoaded = (attachNotifier, store) => {
  console.debug('P: Notifier: Registering "pageLoaded" notifier');

  attachNotifier(LOADED, () => {
    console.debug('P: Notifier: Dispatching "pageLoaded" action');

    store.dispatch(wasLoaded());

    Promise.all([
      auiDetector(),
      getVersion()
    ]).then(([hasAuiResult, auiVersion]) => {
      store.dispatch(hasAui(hasAuiResult));
      store.dispatch(setVersion(auiVersion));
    });
  });
};

export default pageLoaded;

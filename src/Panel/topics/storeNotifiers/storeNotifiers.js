import notifyAboutDeprecation from './deprecation';
import contentScriptInjected from './contentScriptInjected';
import pageLoading from './pageLoading';
import pageLoaded from './pageLoaded';

export default [
  notifyAboutDeprecation,
  contentScriptInjected,
  pageLoading,
  pageLoaded
];

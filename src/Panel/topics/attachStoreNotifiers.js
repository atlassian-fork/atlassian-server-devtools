import storeNotifiers from './storeNotifiers/storeNotifiers';
import { on } from '../portConnection';

export default store => (
  storeNotifiers.map(attachNotifier => attachNotifier(on, store))
);

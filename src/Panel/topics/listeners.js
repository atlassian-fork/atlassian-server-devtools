const listeners = [];

export const registerListener = (topic, callback) => {
  listeners.push({ topic, callback });
};

export const getListeners = () => listeners;

const evaluateCodeInInspectedWindow = (code) => {
  console.debug(`P: Evaluating code "${code}" in CS`);

  return new Promise((resolve, reject) => (
    // TODO: Extract Chrome API for mocking and testing purpose
    chrome.devtools.inspectedWindow.eval(code, (result, exception) => {
      console.debug(`P: Evaluated code "${code}" in CS with result`, result, exception);

      if (exception) {
        reject(exception);
      } else {
        resolve(result);
      }
    })
  ));
};

export default evaluateCodeInInspectedWindow;

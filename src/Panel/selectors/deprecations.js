import { createSelector } from 'reselect';

const getDeprecations = state => state.deprecations.deprecations;
const getDeprecationsFilter = state => state.deprecations.filterQuery;

export const getFilteredDeprecations = createSelector(
  getDeprecations,
  getDeprecationsFilter,
  (deprecations, filter) => {
    if (!filter) {
      return deprecations;
    }

    const filterRegExp = new RegExp(filter, 'i');

    return deprecations
      .filter(deprecation => (
        Object
          .values(deprecation)
          .filter(Boolean)
          .some(value => value.toString().match(filterRegExp))
      ));
  }
);

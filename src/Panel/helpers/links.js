export const getAuiUpgradeGuideLink = version => (
  `https://docs.atlassian.com/aui/${version}/docs/upgrade-guide.html`
);

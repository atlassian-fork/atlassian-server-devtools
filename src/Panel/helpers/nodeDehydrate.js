const nodeDehydrate = (nodeString) => {
  try {
    const wrapper = document.createElement('div');

    wrapper.innerHTML = nodeString;

    return wrapper.lastChild;
  } catch (e) {
    return null;
  }
};

export default nodeDehydrate;

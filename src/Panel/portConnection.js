import { registerListener, getListeners } from '../Panel/topics/listeners';

// TODO: Extract Chrome API for mocking and testing purpose
const portConnection = chrome.runtime.connect({
  name: 'asd_panel'
});

portConnection.postMessage({
  topic: 'INIT_PANEL',
  tabId: chrome.devtools.inspectedWindow.tabId// TODO: Extract Chrome API for mocking and testing purpose
});

portConnection.onMessage.addListener(({ topic, ...message }, port) => {
  console.debug(`P: Connected to port ${port.id || port.name}`);

  const listeners = getListeners();

  console.debug(`P: Received message ${topic}`, message);

  listeners.forEach(({ topic: listenerTopic, callback }) => {
    if (topic === listenerTopic) {
      console.debug(`P: Forwarding message ${topic} to Panel state`, message);
      callback(message);
    }
  });
});

export const emit = (topic, message) => {
  const { tabId } = chrome.devtools.inspectedWindow;// TODO: Extract Chrome API for mocking and testing purpose

  portConnection.postMessage({ ...message, topic, tabId });
};

export const on = (topic, handler) => {
  registerListener(topic, handler);
};

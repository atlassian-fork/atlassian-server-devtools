import React from 'react';
import PropTypes from 'prop-types';
import styles from './Toolbar.less';

const Toolbar = ({ children }) => (
  <div className={styles.Toolbar}>
    {children}
  </div>
);

Toolbar.propTypes = {
  children: PropTypes.node.isRequired
};

export default Toolbar;

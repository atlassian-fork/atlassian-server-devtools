import React from 'react';
import PropTypes from 'prop-types';

import styles from './Console.less';

const Console = ({ children, emptyOutput }) => {
  const output = (children && children.length)
    ? (<div className={styles.ConsoleOutput}>{children}</div>)
    : emptyOutput;

  return (
    <div className={styles.Console}>
      {output}
    </div>
  );
};

Console.propTypes = {
  emptyOutput: PropTypes.node,
  children: PropTypes.node
};

Console.defaultProps = {
  emptyOutput: null,
  children: null
};

export default Console;

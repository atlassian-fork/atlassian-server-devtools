import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import classNamesBind from 'classnames/bind';

import styles from './ConsoleMessage.less';

const classNames = classNamesBind.bind(styles);
const timeFormat = 'HH:mm:ss.SSS';

const ConsoleMessage = ({
  children, showTimestamps, timestamp, anchor, isError, isWarning, className
}) => {
  const formattedTimestamp = showTimestamps ? format(new Date(timestamp), timeFormat) : false;

  const messageClassName = classNames(styles.ConsoleMessageWrapper, {
    ConsoleMessageError: isError,
    ConsoleMessageWarning: isWarning
  }, className);

  const timestampNode = formattedTimestamp
    ? (<span className={styles.ConsoleTimestamp}>{formattedTimestamp}</span>)
    : null;

  return (
    <div className={messageClassName}>
      <div className={styles.ConsoleMessageRow}>
        {timestampNode}

        <span className={styles.ConsoleMessageText}>
          {children}
        </span>

        <span className={styles.ConsoleAnchor}>
          {anchor}
        </span>
      </div>
    </div>
  );
};

ConsoleMessage.propTypes = {
  timestamp: PropTypes.number.isRequired,
  showTimestamps: PropTypes.bool,
  children: PropTypes.node.isRequired,
  anchor: PropTypes.node,
  isError: PropTypes.bool,
  isWarning: PropTypes.bool,
  className: PropTypes.string
};

ConsoleMessage.defaultProps = {
  showTimestamps: false,
  anchor: null,
  isError: false,
  isWarning: false,
  className: null
};

export default ConsoleMessage;

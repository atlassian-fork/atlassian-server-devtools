import React from 'react';
import PropTypes from 'prop-types';
import styles from './PanelWrapper.less';

const PanelWrapper = ({ children }) => (
  <div className={styles.PanelWrapper}>
    {children}
  </div>
);

PanelWrapper.propTypes = {
  children: PropTypes.node.isRequired
};

export default PanelWrapper;

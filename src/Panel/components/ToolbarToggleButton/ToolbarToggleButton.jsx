import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './ToolbarToggleButton.less';

const ToolbarToggleButton = ({ children, className, checked, onToggle }) => {
  const checkboxProps = checked ? { checked } : {};

  return (
    <label className={classNames(styles.ToolbarToggleButton, className)} onChange={onToggle}>
      <input type="checkbox" {...checkboxProps} />
      <span className={styles.Label}>{children}</span>
    </label>
  );
};

ToolbarToggleButton.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  className: PropTypes.string,
  checked: PropTypes.bool,
  onToggle: PropTypes.func.isRequired
};

ToolbarToggleButton.defaultProps = {
  title: null,
  className: null,
  checked: false
};

export default ToolbarToggleButton;

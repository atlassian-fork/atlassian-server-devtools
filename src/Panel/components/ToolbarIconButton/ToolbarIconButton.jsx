import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import ToolbarButton from '../ToolbarButton/ToolbarButton';
import styles from './ToolbarIconButton.less';

export const ClearIcon = 'ClearIcon';

const ToolbarIconButton = ({ icon, onClick, title }) => {
  const className = classNames(styles.ToolbarIconButton, styles[icon]);

  return (
    <ToolbarButton onClick={onClick} alt={title} title={title}>
      <span className={className} />
    </ToolbarButton>
  );
};

ToolbarIconButton.propTypes = {
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default ToolbarIconButton;

import React from 'react';
import PropTypes from 'prop-types';

import ToolbarItem from '../ToolbarItem/ToolbarItem';
import styles from './ToolbarFields.less';

const ToolbarField = ({ onChange, placeholder, defaultValue }) => (
  <ToolbarItem className={styles.ToolbarFieldWrapper}>
    <input
      type="text"
      onChange={(event) => {
        const { value } = event.target;

        onChange(value);
      }}
      placeholder={placeholder}
      defaultValue={defaultValue}
      className={styles.ToolbarField}
    />
  </ToolbarItem>
);

ToolbarField.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  defaultValue: PropTypes.string
};

ToolbarField.defaultProps = {
  defaultValue: ''
};

export default ToolbarField;

import React from 'react';
import PropTypes from 'prop-types';
import styles from './SplitPanel.less';

const SplitPanel = ({ children }) => (
  <div className={styles.SplitPanel}>
    {children}

    <div className={styles.Resize} />
  </div>
);

SplitPanel.propTypes = {
  children: PropTypes.node.isRequired
};

export default SplitPanel;

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './ToolbarItem.less';

const ToolbarItem = ({ children, className }) => (
  <span className={classNames(styles.ToolbarItem, className)}>
    {children}
  </span>
);

ToolbarItem.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

ToolbarItem.defaultProps = {
  children: null,
  className: null
};

export default ToolbarItem;

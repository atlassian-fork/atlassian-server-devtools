import React from 'react';
import PropTypes from 'prop-types';
import classNamesBind from 'classnames/bind';

import styles from './SidebarActionItem.less';

const classNames = classNamesBind.bind(styles);

const SidebarActionItem = ({ active, onClick, children }) => (
  // eslint-disable-next-line
  <li
    className={classNames(styles.SidebarActionItem, { Active: active })}
    onClick={onClick}
  >
    {children}
  </li>
);

SidebarActionItem.propTypes = {
  active: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired
};

SidebarActionItem.defaultProps = {
  active: false
};

export default SidebarActionItem;

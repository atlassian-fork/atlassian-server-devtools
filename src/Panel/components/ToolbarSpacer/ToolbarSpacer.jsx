import React from 'react';
import styles from './ToolbarSpacer.less';

const ToolbarSpacer = () => (
  <span className={styles.ToolbarSpacer} />
);

export default ToolbarSpacer;

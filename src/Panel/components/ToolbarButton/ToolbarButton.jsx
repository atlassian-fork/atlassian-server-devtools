import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './ToolbarButton.less';

const ToolbarButton = ({ children, className, onClick }) => (
  <button onClick={onClick} className={classNames(styles.ToolbarButton, className)}>
    {children}
  </button>
);

ToolbarButton.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired
};

ToolbarButton.defaultProps = {
  children: null,
  className: null
};

export default ToolbarButton;
